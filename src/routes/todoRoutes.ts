import express from 'express';
import {
  createTodo,
  deleteOneTodo,
  getAllTodos,
  getOneTodo,
  updateOneTodo,
} from '../controllers/todosController';

const TodoRouter = express.Router();

TodoRouter.get('/get/all', getAllTodos);
TodoRouter.get('/get/one/:id', getOneTodo);
TodoRouter.post('/create', createTodo);
TodoRouter.put('/update/:id', updateOneTodo);
TodoRouter.delete('/delete/:id', deleteOneTodo);

export default TodoRouter;
