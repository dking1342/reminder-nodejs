import express from 'express';
import {
  createList,
  deleteOneList,
  getAllLists,
  getOneList,
  updateOneList,
} from '../controllers/listsController';

const ListRouter = express.Router();

ListRouter.get('/get/all', getAllLists);
ListRouter.get('/get/one/:id', getOneList);
ListRouter.post('/create', createList);
ListRouter.put('/update/:id', updateOneList);
ListRouter.delete('/delete/:id', deleteOneList);

export default ListRouter;
