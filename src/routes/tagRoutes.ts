import express from 'express';
import {
  createTag,
  deleteOneTag,
  getAllTags,
  getOneTag,
  updateOneTag,
} from '../controllers/tagsController';

const TagRouter = express.Router();

TagRouter.get('/get/all', getAllTags);
TagRouter.get('/get/one/:id', getOneTag);
TagRouter.post('/create', createTag);
TagRouter.put('/update/:id', updateOneTag);
TagRouter.delete('/delete/:id', deleteOneTag);

export default TagRouter;
