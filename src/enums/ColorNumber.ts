export enum ColorNumber {
  TWO = '200',
  THREE = '300',
  FOUR = '400',
  FIVE = '500',
  SIX = '600',
  SEVEN = '700',
  EIGHT = '800',
}
