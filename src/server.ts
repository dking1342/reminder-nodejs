import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import { mongoStartup } from './config/mongodb';
import ListRouter from './routes/listRoutes';
import TagRouter from './routes/tagRoutes';
import TodoRouter from './routes/todoRoutes';

dotenv.config();

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// db init
mongoStartup();

// routes
app.use('/api/todos', TodoRouter);
app.use('/api/tags', TagRouter);
app.use('/api/lists', ListRouter);

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`server listening on port ${PORT}...`);
});
