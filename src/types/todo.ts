import mongoose from 'mongoose';

export type TodoType = {
  name: String;
  tags?: mongoose.Schema.Types.ObjectId[];
  isDone: Boolean;
  isFlagged: Boolean;
  list: mongoose.Schema.Types.ObjectId;
};
