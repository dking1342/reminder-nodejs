export type TagType = {
  name: String;
  isSelected: Boolean;
};
