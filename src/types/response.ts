import HttpStatusCode from 'src/enums/HttpStatusCode';

export type Res<T> = {
  timestamp: Date;
  count?: Number;
  status: HttpStatusCode;
  error?: String;
  message?: String;
  data?: T[] | T;
  developerMessage?: any;
};
