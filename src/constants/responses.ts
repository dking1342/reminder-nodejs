import HttpStatusCode from '../enums/HttpStatusCode';
import { Res } from 'src/types/response';
import { TodoType } from '../types/todo';
import { TagType } from '../types/tag';
import { ListType } from '../types/list';

export const todoResponse: Res<TodoType> = {
  timestamp: new Date(),
  status: HttpStatusCode.OK,
};

export const tagResponse: Res<TagType> = {
  timestamp: new Date(),
  status: HttpStatusCode.OK,
};

export const listResponse: Res<ListType> = {
  timestamp: new Date(),
  status: HttpStatusCode.OK,
};
