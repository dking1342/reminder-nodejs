import HttpStatusCode from '../enums/HttpStatusCode';
import {
  listResponse,
  tagResponse,
  todoResponse,
} from '../constants/responses';
import { Response } from 'express';
import { Res } from '../types/response';
import { TodoType } from '../types/todo';
import { TagType } from '../types/tag';
import { ListType } from '../types/list';

type ErrorType = 'todo' | 'tag' | 'list';

export const errorResponseMessage = (
  error: unknown,
  res: Response,
  type: ErrorType
) => {
  const e = error as Error;
  switch (type) {
    case 'todo':
      const todoRes: Res<TodoType> = {
        ...todoResponse,
        error: e.message,
        status: HttpStatusCode.BAD_REQUEST,
      };
      res.json(todoRes);
      break;
    case 'tag':
      const tagRes: Res<TagType> = {
        ...tagResponse,
        error: e.message,
        status: HttpStatusCode.BAD_REQUEST,
      };
      res.json(tagRes);
      break;
    case 'list':
      const listRes: Res<ListType> = {
        ...listResponse,
        error: e.message,
        status: HttpStatusCode.BAD_REQUEST,
      };
      res.json(listRes);
      break;
    default:
      res.json({});
      break;
  }
};
