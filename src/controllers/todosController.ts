import { Request, Response } from 'express';
import HttpStatusCode from '../enums/HttpStatusCode';
import { TodoType } from '../types/todo';
import { Res } from '../types/response';
import { todoResponse } from '../constants/responses';
import Todo from '../models/Todo';
import { errorResponseMessage } from '../utils/responseErrors';

export const getAllTodos = async (_: Request, res: Response) => {
  try {
    const todos: TodoType[] = await Todo.find({})
      .populate('tags')
      .populate('list');
    const response: Res<TodoType> = {
      ...todoResponse,
      count: todos.length,
      message: 'Get all todos',
      data: todos,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'todo');
  }
};

export const getOneTodo = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const todo: TodoType | null = await Todo.findById(id)
      .populate('tags')
      .populate('list');
    if (todo) {
      const response: Res<TodoType> = {
        ...todoResponse,
        message: `Get the todo with id of ${id}`,
        data: todo,
      };
      res.json(response);
    } else {
      const err = new Error('Unable to retrieve todo');
      errorResponseMessage(err, res, 'todo');
    }
  } catch (error) {
    errorResponseMessage(error, res, 'todo');
  }
};

export const createTodo = async (req: Request, res: Response) => {
  try {
    const userInput: TodoType = req.body;
    await Todo.create(userInput, (err, result) => {
      if (err) {
        errorResponseMessage(err, res, 'todo');
      } else {
        const todo = result as unknown as TodoType;
        const response: Res<TodoType> = {
          ...todoResponse,
          message: 'Created a todo',
          status: HttpStatusCode.CREATED,
          data: todo,
        };
        res.json(response);
      }
    });
  } catch (error) {
    errorResponseMessage(error, res, 'todo');
  }
};

export const updateOneTodo = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const userInput: TodoType = req.body;
    const todo = await Todo.updateOne({ _id: id }, userInput).populate('tags');

    const response = {
      ...todoResponse,
      message: `Updating todo with id ${id}`,
      status: HttpStatusCode.CREATED,
      data: todo,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'todo');
  }
};

export const deleteOneTodo = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const todo = await Todo.deleteOne({ _id: id });
    const response = {
      ...todoResponse,
      message: `Deleting todo with id ${id}`,
      status: HttpStatusCode.NO_CONTENT,
      data: todo,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'todo');
  }
};
