import { Request, Response } from 'express';
import HttpStatusCode from '../enums/HttpStatusCode';
import { ListType } from '../types/list';
import { Res } from '../types/response';
import { listResponse } from '../constants/responses';
import List from '../models/List';
import { errorResponseMessage } from '../utils/responseErrors';

export const getAllLists = async (_: Request, res: Response) => {
  try {
    const lists = await List.find();
    const response: Res<ListType> = {
      ...listResponse,
      count: lists.length,
      message: 'Get all lists',
      data: lists,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'list');
  }
};

export const getOneList = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const list: ListType | null = await List.findById(id);
    if (list) {
      const response: Res<ListType> = {
        ...listResponse,
        message: `Get the list with id of ${id}`,
        data: list,
      };
      res.json(response);
    } else {
      const err = new Error('Unable to retrieve list');
      errorResponseMessage(err, res, 'list');
    }
  } catch (error) {
    errorResponseMessage(error, res, 'list');
  }
};

export const createList = async (req: Request, res: Response) => {
  try {
    const userInput: ListType = req.body;
    await List.create(userInput, (err, result) => {
      if (err) {
        errorResponseMessage(err, res, 'list');
      } else {
        const response: Res<ListType> = {
          ...listResponse,
          message: 'Created a list',
          status: HttpStatusCode.CREATED,
          data: result,
        };
        res.json(response);
      }
    });
  } catch (error) {
    errorResponseMessage(error, res, 'list');
  }
};

export const updateOneList = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const userInput: ListType = req.body;
    const list = await List.updateOne({ _id: id }, userInput);
    const response: Res<ListType> = {
      ...listResponse,
      message: `Updating list with id ${id}`,
      status: HttpStatusCode.CREATED,
      developerMessage: list,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'list');
  }
};

export const deleteOneList = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const list = await List.deleteOne({ _id: id });
    const response: Res<ListType> = {
      ...listResponse,
      message: `Deleting list with id ${id}`,
      status: HttpStatusCode.NO_CONTENT,
      developerMessage: list,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'list');
  }
};
