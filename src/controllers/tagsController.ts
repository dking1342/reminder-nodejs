import { Request, Response } from 'express';
import { tagResponse } from '../constants/responses';
import HttpStatusCode from '../enums/HttpStatusCode';
import Tag from '../models/Tag';
import { Res } from '../types/response';
import { TagType } from '../types/tag';
import { errorResponseMessage } from '../utils/responseErrors';

export const getAllTags = async (_: Request, res: Response) => {
  try {
    const tags = await Tag.find();
    const response: Res<TagType> = {
      ...tagResponse,
      count: tags.length,
      message: 'Get all tags',
      data: tags,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'tag');
  }
};

export const getOneTag = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const tag: TagType | null = await Tag.findById(id);
    if (tag) {
      const response = {
        ...tagResponse,
        message: `Get the tag with id ${id}`,
        data: tag,
      };
      res.json(response);
    } else {
      const err = new Error('Unable to retrieve tag');
      errorResponseMessage(err, res, 'tag');
    }
  } catch (error) {
    errorResponseMessage(error, res, 'tag');
  }
};

export const createTag = async (req: Request, res: Response) => {
  try {
    const userInput: TagType = req.body;
    await Tag.create(userInput, (err, result) => {
      if (err) {
        errorResponseMessage(err, res, 'tag');
      } else {
        const response: Res<TagType> = {
          ...tagResponse,
          message: 'Created a tag',
          status: HttpStatusCode.CREATED,
          data: result,
        };
        res.json(response);
      }
    });
  } catch (error) {
    errorResponseMessage(error, res, 'tag');
  }
};

export const updateOneTag = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const userInput: TagType = req.body;
    const tag = await Tag.updateOne({ _id: id }, userInput);
    const response: Res<TagType> = {
      ...tagResponse,
      message: `Updating tag with id ${id}`,
      status: HttpStatusCode.CREATED,
      developerMessage: tag,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'tag');
  }
};

export const deleteOneTag = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const tag = await Tag.deleteOne({ _id: id });
    const response: Res<TagType> = {
      ...tagResponse,
      message: `Deleting tag with id ${id}`,
      status: HttpStatusCode.NO_CONTENT,
      developerMessage: tag,
    };
    res.json(response);
  } catch (error) {
    errorResponseMessage(error, res, 'tag');
  }
};
