import 'dotenv/config';
import mongoose from 'mongoose';

export const mongoStartup = async () => {
  const conn = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_IP}/${process.env.MONGO_DB}?authSource=admin`;
  // const conn = `${process.env.MONGO_URI}`;

  mongoose
    .connect(conn)
    .then(() => console.log('db connected!'))
    .catch((err) => {
      const error = err as Error;
      console.log('DB error: ', error.stack);
      setTimeout(mongoStartup, 5000); // not best practice but tries to restart if error
    });
};
