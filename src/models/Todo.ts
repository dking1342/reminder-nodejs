import { model, Schema, Types } from 'mongoose';
import List from './List';
import Tag from './Tag';

const TodoSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    tags: [
      {
        type: Types.ObjectId,
        ref: Tag,
        required: false,
      },
    ],
    isFlagged: {
      type: Boolean,
      required: true,
      default: false,
    },
    isDone: {
      type: Boolean,
      required: true,
      default: false,
    },
    list: {
      type: Types.ObjectId,
      ref: List,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Todo = model('Todo', TodoSchema);
export default Todo;
