import { model, Schema } from 'mongoose';

export const TagSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    isSelected: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

const Tag = model('Tag', TagSchema);
export default Tag;
