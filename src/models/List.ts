import { model, Schema } from 'mongoose';

const ListSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    color: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const List = model('List', ListSchema);
export default List;
